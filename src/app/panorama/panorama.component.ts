import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import SphericalImage from 'spherical-image';

@Component({
  selector: 'app-panorama',
  templateUrl: './panorama.component.html',
  styleUrls: ['./panorama.component.scss']
})

export class PanoramaComponent implements AfterViewInit {
  @ViewChild('panorama', {static: false}) el: ElementRef;
  public context: CanvasRenderingContext2D;

  ngAfterViewInit() {
    // child is set
    new SphericalImage(
      this.el.nativeElement,
      'assets/pano2.jpg'
    );
  }
}
